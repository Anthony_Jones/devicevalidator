import ivanivanton.DeviceXML.DOM.DOMDevicesValidator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class DOMValidatorExample {

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        DOMDevicesValidator devicesValidator = new DOMDevicesValidator("resources/DevicesValid.xml",
                "out/DOMoutValid.txt" );
        devicesValidator.validateFile();

        devicesValidator.setXmlFilename("resources/DevicesInvalid.xml");
        devicesValidator.setOutputFileWriter("out/DOMoutInvalid.txt");
        devicesValidator.validateFile();
    }
}