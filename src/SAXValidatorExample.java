import ivanivanton.DeviceXML.SAX.SAXDeviceDefaultHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class SAXValidatorExample {

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {

        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser saxParser = spf.newSAXParser();

        XMLReader xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler(new SAXDeviceDefaultHandler("out/SAXoutInvalid.txt"));
        xmlReader.parse("resources/DevicesInvalid.xml");

        xmlReader.setContentHandler(new SAXDeviceDefaultHandler("out/SAXoutValid.txt"));
        xmlReader.parse("resources/DevicesValid.xml");


    }
}
