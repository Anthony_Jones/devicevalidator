package ivanivanton.DeviceXML;

import java.util.ArrayList;

public class Component {

    public String Name;
    public String Origin;
    public String Price;
    public ArrayList<String> Types;
    public boolean Critical;

    public Component() {
       Types = new ArrayList<>();
       Critical = false;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public void addType(String type) {
        Types.add(type);
    }

    public void setCritical(boolean critical) {
        Critical = critical;
    }

    public void setProperty(String property, String value) {
        switch (property) {
            case "Name":
                this.setName(value);
                break;
            case "Origin":
                this.setOrigin(value);
                break;
            case "Price":
                this.setPrice(value);
                break;
            case "Type":
                this.addType(value);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + property);
        }
    }

    @Override
    public String toString() {
        String critical = "";
        if(this.Critical) {
            critical = "\t\t" +"Critical" +"\n";
        }
        return "Component{\n" +
                "\t\tName='" + Name + "'\n" +
                "\t\tOrigin='" + Origin + "'\n" +
                "\t\tPrice='" + Price + "'\n" +
                "\t\tTypes=" + Types + "\n" +
                critical +
                "\t}";
    }

    public Object getProperty(String property) {
        switch (property) {
            case "Name":
                return this.Name;
            case "Origin":
                return this.Origin;
            case "Price":
                return this.Price;
            case "Type":
                return this.Types;
            case "Critical":
                return this.Critical;
            default:
                throw new IllegalStateException("Unexpected value: " + property);
        }
    }
}
