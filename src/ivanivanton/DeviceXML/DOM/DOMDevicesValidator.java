package ivanivanton.DeviceXML.DOM;

import ivanivanton.DeviceXML.Component;
import ivanivanton.DeviceXML.Device;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Pattern;

public class DOMDevicesValidator {
    private String xmlFilename;
    private FileWriter outputFileWriter;

    public void setXmlFilename(String xmlFilename) {
        this.xmlFilename = xmlFilename;
    }

    public void setOutputFileWriter(String outputFilename) throws IOException {
        File file = new File(outputFilename);
        if (file.createNewFile()) {
            System.out.println("File created: " + file.getName());
        } else {
            System.out.println("File '" + file.getName() + "'already exists.");
        }
        outputFileWriter = new FileWriter(outputFilename);
    }

    public DOMDevicesValidator(String xmlFilename, String outputFilename) throws IOException {
        this.xmlFilename = xmlFilename;
        File file = new File(outputFilename);
        if (file.createNewFile()) {
            System.out.println("Output file created: '" + file.getName() + "'");
        } else {
            System.out.println("Output file '" + file.getName() + "'already exists.");
        }
        outputFileWriter = new FileWriter(outputFilename);
    }


     public void validateFile() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        File fl = new File(xmlFilename);
        Document doc = builder.parse(fl);
        Element root = doc.getDocumentElement();
        String sroot = root.getTagName();

        if (!Objects.equals(sroot, "Devices")) {
            outputToFile("Root element should be named Devices", 0);
        }

        NodeList devicesNodeList = root.getChildNodes();
        ArrayList<Device> deviceArrayList = new ArrayList<>();
        for (int i = 0; i < devicesNodeList.getLength(); i++) {
            Node deviceNode = devicesNodeList.item(i);
            this.processDevice(deviceNode, root, deviceArrayList);
        }
        outputToFile(deviceArrayList.toString(), 0);
        outputFileWriter.close();
    }

    public void processDevice(Node currentNode, Node parentNode, ArrayList<Device> deviceArrayList) {
        short nodeType = currentNode.getNodeType();
        if (nodeType != Node.ELEMENT_NODE) {
            this.elseNodeTypeCallback(currentNode, parentNode, nodeType, 1);
            return;
        }
        Element currentElement = (Element) currentNode;
        if (!Objects.equals(currentElement.getTagName(), "Device")) {
            outputToFile(currentElement.getTagName() + ": element should be named Device", 1);
        }
        if (currentNode.hasAttributes()) {
            outputToFile(currentElement.getTagName() + ": shouldn't have attributes", 1);
        }

        Device device = new Device();
        NodeList componentsNodeList = currentElement.getChildNodes();
        for (int k = 0; k < componentsNodeList.getLength(); k++) {
            Node componentNode = componentsNodeList.item(k);
            this.processComponent(componentNode, currentElement, device);
        }
        deviceArrayList.add(device);
        outputToFile("", 0);
    }


    public void processComponent(Node currentNode, Element parentElement, Device device) {
        short nodeType = currentNode.getNodeType();
        if (nodeType != Node.ELEMENT_NODE) {
            this.elseNodeTypeCallback(currentNode, parentElement, nodeType, 2);
            return;
        }
        HashMap<String, Integer> errors = new HashMap<>();
        Element currentElement = (Element) currentNode;
        if (!Objects.equals(currentElement.getTagName(), "Component")) {
            outputToFile(currentElement.getTagName() + ": element should be named Component", 2);
        }
        if (currentNode.hasAttributes()) {
            outputToFile(currentElement.getTagName() + ": element shouldn't have attributes", 2);
        }

        Component component = new Component();
        NodeList componentChildren = currentElement.getChildNodes();
        HashMap<String, Integer> check = new HashMap<>();
        check.put("Name", 0);
        check.put("Origin", 0);
        check.put("Price", 0);
        check.put("Critical", 0);
        check.put("Type", 0);
        for (int j = 0; j < componentChildren.getLength(); j++) {
            Node childrenNode = componentChildren.item(j);
            this.processComponentChild(childrenNode, currentElement, component, check);
        }
        if (check.get("Name") == 0) {
            outputToFile(currentElement.getTagName() + ": there are should be Name element", 3);
        } else if (check.get("Name") > 1) {
            outputToFile(currentElement.getTagName() + ": there are should be only one Name element", 3);
        }
        if (check.get("Origin") == 0) {
            outputToFile(currentElement.getTagName() + ": there are should be Origin element", 3);
        } else if (check.get("Origin") > 1) {
            outputToFile(currentElement.getTagName() + ": there are should be only one Origin element", 3);
        }
        if (check.get("Price") == 0) {
            outputToFile(currentElement.getTagName() + ": there are should be Price element", 3);
        } else if (check.get("Price") > 1) {
            outputToFile(currentElement.getTagName() + ": there are should be only one Price element", 3);
        }
        if (check.get("Critical") > 1) {
            outputToFile(currentElement.getTagName() + ": there are should be only one Critical element", 3);
        }
        device.addComponent(component);
        outputToFile("", 0);
    }

    public void processComponentChild(Node currentNode, Element parentElement, Component component,
                                      HashMap<String, Integer> componentChildrenQuantity) {
        short nodeType = currentNode.getNodeType();
        if (nodeType != Node.ELEMENT_NODE) {
            this.elseNodeTypeCallback(currentNode, parentElement, nodeType, 3);
            return;
        }
        Element currentElement = (Element) currentNode;
        if (currentNode.hasAttributes()) {
            outputToFile(currentElement.getTagName() + ": shouldn't have attributes", 3);
        }

        switch (currentElement.getTagName()) {
            case "Name":
            case "Origin":
            case "Price":
            case "Type":
                componentChildrenQuantity.put(currentElement.getTagName(),
                        componentChildrenQuantity.get(currentElement.getTagName()) + 1);
                NodeList textList = currentElement.getChildNodes();
                for (int l = 0; l < textList.getLength(); l++) {
                    Node textNode = textList.item(l);
                    if (textNode.hasAttributes()) {
                        outputToFile(textNode.getNodeName() + " shouldn't have attributes", 3);
                    }
                    if (textNode.getNodeType() == Node.TEXT_NODE) {
                        Text text = (Text) textNode;
                        component.setProperty(currentElement.getTagName(), text.getWholeText());
                        if (Objects.equals(currentElement.getTagName(), "Price")) {
                            String decimalPattern = "([0-9]*)\\.([0-9]+)";
                            if (!Pattern.matches(decimalPattern, text.getWholeText())) {
                                outputToFile(currentElement.getTagName() + ": should be float/double", 3);
                            }
                        }
                    } else {
                        outputToFile(currentElement.getTagName() + " there are should be only text", 3);
                    }
                }
                break;
            case "Critical":
                componentChildrenQuantity.put("Critical", componentChildrenQuantity.get("Critical") + 1);
                if (currentElement.hasChildNodes()) {
                    outputToFile("Critical: should not contain text", 3);
                }
                component.setCritical(true);
                break;
            default:
                outputToFile(parentElement.getNodeName() + " there are shouldn't be elements named \"" +
                        currentElement.getTagName() + "\"", 3);
        }
    }

    private void elseNodeTypeCallback(Node currentNode, Node parentNode, short nodeType, int treeLevel) {
        if (nodeType == Node.TEXT_NODE) {
            Text deviceText = (Text) currentNode;
            boolean isWhitespace = deviceText.getWholeText().trim().length() == 0;
            if (!isWhitespace) {
                outputToFile(parentNode.getNodeName() + ": there is should be only Device element", treeLevel);
            }
        } else {
            outputToFile(parentNode.getNodeName() + ": there is should be only Device element", treeLevel);
        }
    }

    private void outputToFile(String text, int treeLevel) {
        StringBuilder tabs = new StringBuilder();
        for (int i = 0; i < treeLevel; i++) {
            tabs.append('\t');
        }
        try {
            outputFileWriter.write(tabs + text + '\n');
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
