package ivanivanton.DeviceXML;

import java.util.ArrayList;

public class Device {
    private final ArrayList<Component> components;

    public Device() {
        components = new ArrayList<>();
    }
    public void addComponent(Component component) {
        components.add(component);
    }

    public Component peek() throws Exception {
        if(!components.isEmpty()) {
            return components.get(components.size() - 1);
        } else {
            throw new Exception("components is empty");
        }
    }

    @Override
    public String toString() {
        StringBuilder components = new StringBuilder();
        for (Component component:
             this.components) {
            components.append("\n\t").append(component.toString());
        }
        return "Device{" +
                "" + components + "\n\n" +
                '}';
    }
}
