package ivanivanton.DeviceXML.SAX;

import ivanivanton.DeviceXML.Component;
import ivanivanton.DeviceXML.Device;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Stack;

public class SAXDeviceDefaultHandler extends DefaultHandler {

    private FileWriter outputFileWriter;
    private Stack<Device> devices;
    private int treeLevel = 0;

    private StringBuilder errors;
    private boolean hasErrors = false;
    private String currentTagName;


    public SAXDeviceDefaultHandler(String outputFilename) throws IOException {
        File file = new File(outputFilename);
        if (file.createNewFile()) {
            System.out.println("Output file created: '" + file.getName() + "'");
        } else {
            System.out.println("Output file '" + file.getName() + "'already exists.");
        }
        outputFileWriter = new FileWriter(outputFilename);
    }

    @Override
    public void startDocument() throws SAXException {
        devices = new Stack<>();
        errors = new StringBuilder();
    }

    @Override
    public void endDocument() throws SAXException {
        if(hasErrors) {
            try {
                outputFileWriter.write(errors.toString());
                outputFileWriter.write("\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            outputFileWriter.write(devices.toString());
            outputFileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "Devices":
                treeLevel = 0;
                break;
            case "Device":
                devices.push(new Device());
                treeLevel = 1;
                break;
            case "Component":
                devices.peek().addComponent(new Component());
                treeLevel = 2;
                break;
            case "Name":
            case "Origin":
            case "Type":
            case "Critical":
                treeLevel = 3;
                break;
        }
        currentTagName = qName;
        outputToString(qName, treeLevel, true);
        if(attributes.getLength() != 0 && !qName.equals("Devices")) {
            outputError(":shouldn't have attributes", treeLevel + 1);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String nodeText = (new String(ch, start, length)).trim();
        switch (currentTagName) {
            case "Name":
            case "Origin":
            case "Price":
                try {
                    if(nodeText.length() != 0) {
                        Component lastComponentOfLastDevice = devices.peek().peek();
                        if (lastComponentOfLastDevice.getProperty(currentTagName) != null) {
                            outputError(":there are could be only one " + currentTagName + " element", treeLevel + 1);
                        }
                        lastComponentOfLastDevice.setProperty(currentTagName, new String(ch, start, length));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "Type":
                try {
                    if(nodeText.length() != 0) {
                        Component lastComponentOfLastDevice = devices.peek().peek();
                        lastComponentOfLastDevice.setProperty(currentTagName, new String(ch, start, length));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "Critical":
                try {
                    Component lastComponentOfLastDevice = devices.peek().peek();
                    if ((Boolean)lastComponentOfLastDevice.getProperty(currentTagName)) {
                        outputError(":there are could be only one " + currentTagName + " element", treeLevel + 1);
                    }
                    lastComponentOfLastDevice.setCritical(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(nodeText.length() != 0) {
                    outputError(":shouldn't contain text", treeLevel + 1);
                }
                currentTagName = "";
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("Component")) {
            try {
                Component lastComponentOfLastDevice = devices.peek().peek();
                if(lastComponentOfLastDevice.getProperty("Name") == null) {
                    outputError(":there is should be at least one Name element", treeLevel);
                }
                if(lastComponentOfLastDevice.getProperty("Origin") == null) {
                    outputError(":there is should be at least one Origin element", treeLevel);
                }
                if(lastComponentOfLastDevice.getProperty("Price") == null) {
                    outputError(":there is should be at least one Price element", treeLevel);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Empty tags produce such sequence: "start->end->characters"
        //Non empty: "start->characters->end"
        if (!qName.equals("Critical")) {
            currentTagName = "";
        }
    }

    private void outputToString(String text, int treeLevel, boolean newLine) {
        StringBuilder tabs = new StringBuilder();
        for (int i = 0; i < treeLevel; i++) {
            tabs.append('\t');
        }
        errors.append(tabs).append(text);
        if(newLine) {
            errors.append('\n');
        }
    }

    private void outputError(String text, int treeLevel) {
        hasErrors = true;
        outputToString("#", 0, false);
        outputToString(text, treeLevel, true);
    }
}
